package kodutoo;

/* Klassis Aken valmistasin akna, mille sees on 
 * lahtrid sisestamiseks ja nupp, 
 * mis arvutab vastuse
 * Klassi "Aken" valmistamisel kasutasin ma allikana YouTube videoid,
 * https://www.youtube.com/watch?v=FLkOX4Eez6o&list=PL6gx4Cwl9DGBzfXLWLSYVy8EbTdpGbUIG
 * @author: Ragnar R�stas
 */

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Aken extends Application {

	// Muutsin nende kasutamise s�nu, et oleks lihtsam, edasiselt kasutada.
	static Stage aken;
	Button nupp;

	public static void main(String[] args) {
		launch(args);

	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		// M��rasin aknale tema nime.
		aken = primaryStage;
		aken.setTitle("Valuuta arvutaja");

		// Paigutasin lahtrite �ldised asukohad.
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10, 10, 10, 10));
		grid.setVgap(5);
		grid.setHgap(5);
		grid.setAlignment(Pos.CENTER);

		// M��ran esimese lahtri nime.
		Label valuuta1 = new Label("Kurss 1");
		GridPane.setConstraints(valuuta1, 1, 2);

		// Tegin lahtri.
		TextField valuuta11 = new TextField();
		valuuta11.inputMethodRequestsProperty();
		valuuta11.setPromptText("Kurss 1");
		GridPane.setConstraints(valuuta11, 1, 3);

		// M��ran teise lahtri nime.
		Label valuuta2 = new Label("Kurss 2");
		GridPane.setConstraints(valuuta2, 2, 2);

		// Valmistasin teise lahtri.
		TextField valuuta22 = new TextField();
		valuuta22.inputMethodRequestsProperty();
		valuuta22.setPromptText("Kurss 2");
		GridPane.setConstraints(valuuta22, 2, 3);

		// M��rasin kolmanda lahtri nime.
		Label arv = new Label("Sisesta arv");
		GridPane.setConstraints(arv, 1, 5);

		// Tegin kolmanda lahtri.
		TextField arv2 = new TextField();
		arv2.inputMethodRequestsProperty();
		arv2.setPromptText("Sisesta arv");
		GridPane.setConstraints(arv2, 1, 6);

		// N�itab, mis kursse saab arvutada.
		Label kursid = new Label("Kursid: euro, dollar, nael, jeen, rubla.");
		GridPane.setConstraints(kursid, 1, 0);

		// Valmistasin nupu ning m��rasin temale funktsiooni, kasutades lambda
		// expression-it,
		// mis lihtsustab hulgaliselt koodi arusaama ja �lesehitust.
		Button arvutaNupp = new Button("Arvuta");

		arvutaNupp.setOnAction(e -> {
			double arv3 = Double.parseDouble(arv2.getText());
			Text vastus = new Text("Vastuseks on " + Arvutamine.main(valuuta11.getText(), valuuta22.getText(), arv3)
					+ " " + valuuta22.getText());

			GridPane.setConstraints(vastus, 1, 10);
			grid.getChildren().remove(vastus);
			grid.getChildren().add(vastus);

		});

		// Siin m��rasin ma "Arvuta nupu" asukoha ja lisasin "Aknale" k�ik
		// vajalikud nimed, lahtrid ja nupud.
		GridPane.setConstraints(arvutaNupp, 1, 7);
		grid.getChildren().addAll(valuuta1, valuuta11, valuuta2, valuuta22, arvutaNupp, arv, arv2, kursid);

		// M��rasin kogu "Akna" suuruse.
		Scene nag = new Scene(grid, 460, 400);
		aken.setScene(nag);
		aken.show();

	}

}