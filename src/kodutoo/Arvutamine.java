package kodutoo;


/* Siin tegin ma arvutamise jaoks eraldi klassi ja meetodi.
 *  @author: Ragnar R�stas
 */

public class Arvutamine {
	public static double main(String esimene, String teine, double sisend) {

		boolean stop = false;
		double vastus = 0.0;

		// Siin tegin ma While loop-i, kus tuleb esimene kurss ja teine kurss sisestada,
		// mille peale ta kontrollib sisestatud andmeid ning arvutab vastuse.
		while (stop == false) {
			double Kurss = Andmed.valuuta(esimene.toUpperCase() + "TO" + teine.toUpperCase());
			if (!(Kurss == 0.0)) {
				vastus = Kurss * sisend;
				stop = true;
			} else {
			}
			break;
		}
		
		// Kui vastus peaks 0 v�rduma siis �tleb program, et ei saa
		// arvutada.
		if (vastus == 0.0) {
			System.out.println("Ei saa arvutada");
		}

		return vastus;
	}

}
