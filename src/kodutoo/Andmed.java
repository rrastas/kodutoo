package kodutoo;

/* 											 author: Ragnar R�stas
 * 
 * 												VALUUTA ARVUTAJA
 * 
 * Kurssid v�tsin google-ist
 */

public class Andmed {

	public static double valuuta(String val) {

		// Teen kursside nimedele massiivi
		String[] Nimed = new String[20];
		// Kursside nimed
		Nimed[0] = "DollarToEuro".toUpperCase();
		Nimed[1] = "EuroToDollar".toUpperCase();
		Nimed[2] = "EuroToNael".toUpperCase();
		Nimed[3] = "NaelToEuro".toUpperCase();
		Nimed[4] = "NaelToDollar".toUpperCase();
		Nimed[5] = "DollarToNael".toUpperCase();
		Nimed[6] = "EuroToRuble".toUpperCase();
		Nimed[7] = "DollarToRubla".toUpperCase();
		Nimed[8] = "NaelToRubla".toUpperCase();
		Nimed[9] = "RublaToEuro".toUpperCase();
		Nimed[10] = "RublaToDollar".toUpperCase();
		Nimed[11] = "RublaToNael".toUpperCase();
		Nimed[12] = "EuroToJeen".toUpperCase();
		Nimed[13] = "DollarToJeen".toUpperCase();
		Nimed[14] = "NaelToJeen".toUpperCase();
		Nimed[15] = "RublaToJeen".toUpperCase();
		Nimed[16] = "JeenToEuro".toUpperCase();
		Nimed[17] = "JeenToDollar".toUpperCase();
		Nimed[18] = "JeenToNael".toUpperCase();
		Nimed[19] = "JeenToRubla".toUpperCase();
		
		

		// Teen massivi et m��rata kursside v��rtused
		double[] Kursid = new double[20];
		// v��rtused
		Kursid[0] = 0.912492016; 		 		    	   // 1 dollari v��rtus eurodes.
		Kursid[1] = 1.0959; 					 		  // 1 euro v��rtus dollarites.
		Kursid[2] = 0.722246021; 	   		   			 // 1 euro v��rtus naelades.
		Kursid[3] = 1.38456976; 	 		    		// 1 poundi v��rtus eurodes.
		Kursid[4] = 1.51735; 		 		   		   // 1 poundi v��rtus dollarites.
		Kursid[5] = 0.659043728; 			    	  // 1 dollari v��rtus naelades.
		Kursid[6] = 79.7506216; 			  		 // 1 euro v��rtus vene rublades.
		Kursid[7] = 73.1314904; 			 		// 1 dollari v��rtus vene rublades.
		Kursid[8] = 108.149042; 		  		   // 1 naela v��rtus vene rublades.
		Kursid[9] = 0.0125390872; 		 		  // 1 vene rubla v��rtus eurodes.
		Kursid[10] = 0.013674; 					 // 1 vene rubla v��rtus dollarites.
		Kursid[11] = 0.00924649892; 			// 1 vene rubla v��rtus naelades.
		Kursid[12] = 129.74539; 			   // 1 euro v��rtus jeenides
		Kursid[13] = 118.9768; 				  // 1 dollari v��rtus jeenides
		Kursid[14] = 175.94646; 			 // 1 naela v��rtus jeenides
		Kursid[15] = 1.62688876; 			// 1 vene rubla v��rtus jeenides.
		Kursid[16] = 0.00770740296; 	   // 1 jeeni v��rtus eurodes.
		Kursid[17] = 0.008405; 			  // 1 jeeni v��rtus dollarites.
		Kursid[18] = 0.00568354713; 	 // 1 jeeni v��rtus naelades.
		Kursid[19] = 0.614670177; 		// 1 jeeni v��rtus vene rublades.

		double tagastus = 0;

		// J�rgnevalt otsivad nimed endale vaste v��rtustest
		for (int i = 0; i < Nimed.length; i++) {
			if (val.equals(Nimed[i])) {
				tagastus = Kursid[i];
			}
		}
		// Kui tagastus peaks 0 tulema siis �eldakse, et kursse ei leitud
		if (tagastus == 0) {
			System.out.println("Kursse ei leitud!");
		}
		return tagastus;
	}

}
